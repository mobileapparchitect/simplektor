package com.arkangelx


import freemarker.cache.ClassTemplateLoader
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.OAuthServerSettings
import io.ktor.auth.UserPasswordCredential
import io.ktor.auth.verifyWithOAuth2
import io.ktor.client.HttpClient
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.features.StatusPages
import io.ktor.freemarker.FreeMarker
import io.ktor.gson.gson
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.locations.Locations
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import kotlinx.coroutines.launch
import java.util.*


fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads

fun Application.module(testing: Boolean = false) {

    install(DefaultHeaders)

    install(StatusPages) {
        exception<Throwable> { e ->
            call.respondText(
                e.localizedMessage,
                ContentType.Text.Plain, HttpStatusCode.InternalServerError
            )
        }
    }

    install(ContentNegotiation) {
        gson()
    }

    install(FreeMarker) {
        templateLoader = ClassTemplateLoader(this::class.java.classLoader, "templates")
    }

    install(Locations)
    var oauth = OAuthServerSettings.OAuth2ServerSettings(
        name = "google",
        authorizeUrl = "https://accounts.google.com/o/oauth2/auth",
        accessTokenUrl = "https://www.googleapis.com/oauth2/v3/token",
        requestMethod = HttpMethod.Post,
        clientId = "935108530234-39pf4kvc9n1dah0lhpj58jg8e25uv55s.apps.googleusercontent.com",
        clientSecret = "8cANFTPL4qher8BGsZyTvFx4",
        defaultScopes = listOf("https://www.googleapis.com/auth/plus.login")
    )

    var userSettings = UserPasswordCredential("info@mobileparadigm.co.uk", "Android60")
    suspend fun login(): String = verifyWithOAuth2(userSettings, HttpClient(), oauth).accessToken
    launch {
          System.out.println(login())
    }


    routing {
        static("/static") {
            resources("images")
        }
        get("/") {
            call.respondText("hello")
        }

    }

}