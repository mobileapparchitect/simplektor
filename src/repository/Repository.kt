package com.arkangelx.webapp.repository

import com.arkangelx.model.Courses


interface Repository {
    suspend fun addCourse(courseId: String, couresTitle: String, courseDescription: String): Courses?
    suspend fun getCourse(courseId: Int, courseTitle: String): Courses?
    suspend fun getAllCourses(): List<Courses>
    suspend fun removeCourse(courseId: Int): Boolean
    suspend fun clear()

}