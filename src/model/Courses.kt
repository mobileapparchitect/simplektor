package com.arkangelx.model


import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.Column
import java.io.Serializable

data class Course(
    val courseId: Int,
    val courseTitle: String,
    val courseDescrition: String
) : Serializable

object Courses : IntIdTable() {
    val courseId: Column<String> = varchar("course_id", 20).index()
    val courseTitle: Column<String> = varchar("course_title", 255)
    val courseDescription: Column<String> = varchar("course_description", 255)
}